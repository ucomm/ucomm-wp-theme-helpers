# WP Theme Helpers

This is meant to be a library of commonly used functions for our themes.

## Usage
To use in a theme project, follow these steps 

- `composer require ucomm/wp-theme-helpers`
- Require the vendor directory in `functions.php`. This should be done for you if you're using our theme boilerplate
- Use the methods in hooks like this

```php
// Add mime types
add_action('upload_mimes', UComm\WPThemeFileTypes::upload_mime_types());

// Register/enable menus
add_action('after_setup_theme', UComm\WPThemeHelpers::register_menus($menus));
```

## Methods Reference

### WPThemeBeaverBuilderHelpers
- `add_fonts_to_beaver_builder`
- `is_builder_active`
- `is_builder_enabled`

### WPThemeFileTypes

- `add_custom_fonts_to_beaver_builder`
- `add_image_sizes`
- `upload_mime_types`

### WPThemeHelpers

- `register_menus`
- `register_sidebar_widgets`