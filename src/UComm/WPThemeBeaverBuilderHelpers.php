<?php

namespace UComm;

class WPThemeBeaverBuilderHelpers {
  /**
   * A wrapper for checking if the beaver builder page builder is active.
   *
   * @return boolean
   */
  public static function is_builder_active() {
    if (class_exists('FLBuilderModel')) {
      return \FLBuilderModel::is_builder_active();
    } else {
      return false;
    }
  }
  
  /**
   * A wrapper for checking if beaver builder is currently being used on a page.
   *
   * @return boolean
   */
  public static function is_builder_enabled() {
    if (class_exists('FLBuilderModel')) {
      return \FLBuilderModel::is_builder_enabled();
    } else {
      return false;
    }
  }

  /**
   * Add custom fonts to the beaver builder system fonts selections.
   * The $custom fonts should take the form - 
   * 
   * $custom_fonts = array(
   *  'Proxima Nova' => array(
   *     'fallback' => 'Verdana, Arial, Helvetica, sans-serif',
   *     'weights' => array(
   *       '300',
   *       '400',
   *       '600',
   *       '700'
   *     )
   *   )
   * );
   *
   * @param array $custom_fonts - an array of arrays of fonts to add.
   * @return void
   */
  static public function add_fonts_to_beaver_builder($custom_fonts = array()) {

    if (!class_exists('FLBuilderModel')) return;

    add_filter('fl_builder_font_families_system', function($system_fonts) use ($custom_fonts) {
      return self::add_custom_fonts($system_fonts, $custom_fonts);
    });
  }

  /**
   * Add custom fonts to the beaver builder system fonts
   *
   * @param array $system_fonts - fonts coming in from beaver builder to be filtered
   * @param array $custom_fonts - additional fonts to add
   * @return array
   */
  static private function add_custom_fonts($system_fonts = array(), $custom_fonts = array()) {

    foreach ($custom_fonts as $font => $settings) {
      $system_fonts[$font] = $settings;
    }
    return $system_fonts;
  }
}