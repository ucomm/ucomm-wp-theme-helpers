<?php

namespace UComm;

class WPThemeHelpers {

  /**
   * Takes an array of nav menus to register 
   * 
   * array(
   *    'slug' => 'Menu Name'
   * )
   *
   * @param array $menus
   * @param string $text_domain
   * @return void
   */
  public static function register_menus($menus = array(), $text_domain = '') {
    foreach ($theme_menus as $slug => $name) {
      register_nav_menu($slug, __($name), $text_domain);
    }
  }

  /**
   * The $widgets array should take the form
   * 
   * array(
   *    array(
   *      'name' => __('Footer Widget 1', 'lobo'),
   *      'id' => 'footer_widget_1',
   *      'before_widget' => '<div class="widget footer-widget">',
   *      'after_widget' => '</div>'
   *    )
   * )
   * 
   * see here for all args -> https://codex.wordpress.org/Function_Reference/register_sidebar
   *
   * @param array $widgets
   * @return void
   */
  public static function register_sidebar_widgets($widgets = array()) {
    foreach ($widgets as $w) {
      register_sidebar($w);
    }
  }
}