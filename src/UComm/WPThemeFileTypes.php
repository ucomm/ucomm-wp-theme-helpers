<?php

namespace UComm;

class WPThemeFileTypes {

  /**
   *  The array should take the form of 
   *  array(
   *    'size-slug' => array(
   *      'readable_name' => __('Name', 'text-domain'),
   *      'width' => Int,
   *      'height' => Int,
   *      'crop' => Bool (default false)
   *    )
   *  )
   * 
   * https://developer.wordpress.org/reference/functions/add_image_size/
   *
   * @param array $image_sizes
   * 
   * @return void
   */
  public static function add_image_sizes($image_sizes = array()) {
    add_theme_support('post-thumbnails');
    foreach ($image_sizes as $name => $size) {
      add_image_size($name, $size['width'], $size['height']);
    }
  }


  /**
   * Allow non-standard filetypes to be uploaded to the WP media library.
   *
   * @param array $existing_mimes
   * @return array
   */
  static public function upload_mime_types($existing_mimes = array()) {
    $existing_mimes['zip'] = 'application/zip';
    $existing_mimes['svg'] = 'image/svg+xml';
    return $existing_mimes;
  }
}